#include <bits/stdc++.h>

using namespace std;

void largestPermutation(vector<int> &nums, int k) {
    int index = 0;
    int swaps = 0;
    while (index < nums.size() && swaps < k) {
        int max = INT_MIN, maxi = index;
        for (int i = index; i < nums.size(); i++) {
            if (nums[i] > max) {
                max = nums[i];
                maxi = i;
            }
        }
        if (maxi != index) {
            int temp = nums[maxi];
            nums[maxi] = nums[index];
            nums[index] = temp;
            swaps++;
        }
        index++;
    }
}

int main() {
    int N, K;
    while (cin >> N >> K) {
        vector<int> nums(N);
        for (int i = 0; i < N; i++) {
            int n;
            cin >> n;
            nums[i] = n;
        }
        if (nums.size() == 1) cout << nums[0] << '\n';
        else {
            largestPermutation(nums, K);
            for (int i = 0; i < nums.size(); i++) {
                cout << nums[i];
                if (i != nums.size() - 1) cout << ' ';
            }
            cout << '\n';
        }
    }
    return 0;
}
