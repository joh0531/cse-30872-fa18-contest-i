#include <bits/stdc++.h>

using namespace std;

int SCORES[] = {2, 3, 7};
set<vector<int>> ANSWERS[101];

void findWays(int i) {
    for (auto s : SCORES) {
        if (i - s > 1) {
            for (auto way : ANSWERS[i - s]) {
                if (way[way.size() - 1] <= s) {
                    way.push_back(s);
                    ANSWERS[i].insert(way);
                }
            }
        }
    }
}

int main() {
    ANSWERS[2] = {{2}};
    ANSWERS[3] = {{3}};
    ANSWERS[7] = {{7}};
    for (int i = 4; i <= 100; i++) {
        findWays(i);
    }
    int p;
    while (cin >> p) {
        if (ANSWERS[p].size() == 1) {
            cout << "There is " << ANSWERS[p].size() << " way to achieve a score of " << p << ":\n";
        } else {
            cout << "There are " << ANSWERS[p].size() << " ways to achieve a score of " << p << ":\n";
        }
        for (auto a : ANSWERS[p]) {
            for (int i = 0; i < a.size(); i++) {
                cout << a[i];
                if (i != a.size() - 1) cout << ' ';
            }
            cout << '\n';
        }
    }
    return 0;
}
