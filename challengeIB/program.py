#!/usr/bin/env python3

import sys
import collections

def areIsomorphic(words):
    if len(words) != 2:
        return False

    charMap1, charMap2 = {},{}

    for loc, c in enumerate(words[0]):
        if c in charMap1:
            charMap1[c].append(loc)
        else:
            charMap1[c] = [loc]

    for loc, c in enumerate(words[1]):
        if c in charMap2:
            charMap2[c].append(loc)
        else:
            charMap2[c] = [loc]
    
    values1 = set(tuple(row) for row in list(charMap1.values()))
    values2 = set(tuple(row) for row in list(charMap2.values()))
    return values1 == values2

if __name__ == '__main__':
    for line in sys.stdin:
        words = line.strip().split()
        if areIsomorphic(words):
            print('Isomorphic')
        else:
            print('Not Isomorphic')
