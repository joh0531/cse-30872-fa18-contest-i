#!/usr/bin/env python3

import sys

def dispBinStrings(binInfo):
    # Construct base bits
    ones = int(binInfo[1])
    zeros = int(binInfo[0]) - ones
    
    bits = []
    for _ in range(ones):
        bits.append(1)
    for _ in range(zeros):
        bits.append(0)

    # find all permutations, treating duplicates as duplicates
    perms = [[]]
    for n in bits:
        curr_perms = []
        for perm in perms:
            for i in range(len(perm) + 1):
                curr_perms.append(perm[:i] + [n] + perm[i:])

                if i < len(perm) and perm[i] == n: break
        perms = curr_perms
    
    # display perms lexographically sorted
    for perm in sorted(perms):
        print(''.join(map(str,perm)))


if __name__ == '__main__':
    binInfos = [line.strip().split() for line in sys.stdin]
    for binInfo in binInfos[:-1]:
        dispBinStrings(binInfo)
        print()
    dispBinStrings(binInfos[-1])
